FROM alpine

RUN apk add -U \
		python \
		py-requests \
		bash \
		shadow \
		sudo \
		git

RUN groupadd -g 1000 build \
 && useradd -u 1000 -g 1000 -m build \
 && echo 'Defaults:build !requiretty' | tee -a /etc/sudoers \
 && echo 'build ALL=(ALL:ALL) NOPASSWD: ALL' | tee -a /etc/sudoers \
 && chown build.build /usr/local/bin

USER build
RUN git clone https://git.icinga.com/build-docker/scripts.git /usr/local/bin
ENTRYPOINT ["/usr/local/bin/icinga-build-entrypoint"]
CMD ["icinga-build-upload-aptly"]
