ifeq ($(DOCKER_IMAGE),)
DOCKER_IMAGE := registry.icinga.com/build-docker/upload
endif

FROM := $(shell grep FROM Dockerfile | cut -d" " -f2)
IMAGE := $(DOCKER_IMAGE):latest

all: pull build

pull:
	docker pull "$(IMAGE)" || true
	docker pull "$(FROM)"

build:
	docker build --cache-from "$(IMAGE)" --tag "$(IMAGE)" .

push:
	docker push "$(IMAGE)"

clean:
	if (docker inspect --type image "$(IMAGE)" >/dev/null 2>&1); then docker rmi "$(IMAGE)"; fi
